<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFilmcastTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('filmcast', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('filmId');
            $table->unsignedBigInteger('castId');

            $table->foreign('filmId')->references('id')->on('film');
            $table->foreign('castId')->references('id')->on('cast');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('filmcast');
    }
}

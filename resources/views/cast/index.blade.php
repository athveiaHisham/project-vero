@extends('adminlte.master')

@section('content-title')

Casts List

@endsection

@section('content')

<div class="card">
    <!-- <div class="card-header">
        <h3 class="card-title">Condensed Full Width Table</h3>
    </div> -->
    <!-- /.card-header -->
    <div class="card-body p-0">
        @if(session()->has('message'))
        <div class="alert alert-success">
            {{ session()->get('message') }}
        </div>
        @endif
        <a class="btn btn-primary" href="/cast/create">Add new cast</a>
        <table class="table table-sm">
            <thead>
                <tr>
                    <th style="width: 10px">#</th>
                    <th>Cast Name</th>
                    <th>Age</th>
                    <th>Detail</th>
                </tr>
            </thead>
            <tbody>
                @forelse($cast as $key => $cast)
                <tr>
                    <td>{{ $key + 1 }}</td>
                    <td>{{ $cast->nama }}</td>
                    <td>{{ $cast->umur }}</td>
                    <td>
                        <a href="/cast/{{ $cast->id }}" class="btn btn-info btn-sm">Show</a>
                        <a href="/cast/{{ $cast->id }}/edit" class="btn btn-success btn-sm">Edit</a>
                        <form action="/cast/{{ $cast->id }}" method="POST">
                            @csrf
                            @method('DELETE')
                            <input type="submit" value="delete" class="btn btn-danger btn-sm">
                        </form>
                    </td>
                </tr>
                @empty
                <tr>
                    <td colspan="4" align="center">No Data Available</td>
                </tr>
                @endforelse
            </tbody>
        </table>
    </div>
    <!-- /.card-body -->
</div>

@endsection
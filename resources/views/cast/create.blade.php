@extends('adminlte.master')

@section('content-title')

Create Cast

@endsection

@section('content')

<div class="card card-primary">
    <div class="card-header">
        <h3 class="card-title">Create Cast</h3>
    </div>
    <!-- /.card-header -->
    <!-- form start -->
    <form action="/cast" method="POST">
        @csrf
        <div class="card-body">
            <div class="form-group">
                <label for="name">Name</label>
                <input type="text" class="form-control" name="cast_name" id="cast_name" value=" {{ old('cast_name', '') }} " placeholder="Enter cast name">
                @error('cast_name')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label for="age">Age</label>
                <input type="text" class="form-control" name="cast_age" id="cast_age" value=" {{ old('cast_age', '') }} " placeholder="Cast Age">
                @error('cast_age')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <!-- /.card-body -->

            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Create</button>
            </div>
    </form>

</div>
@endsection
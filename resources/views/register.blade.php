<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Signup Form</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h2>Sign Up Form</h2>
    <form method="POST" action="/welcome">
        @csrf
        <p><label for="">First Name:</label></p>
        <p><input type="text" name="fname" id="fname"></p>
        <p><label for="">Last Name:</label></p>
        <p><input type="text" name="lname" id="lname"></p>
        <p><label for="">Gender:</label></p>
        <p>
            <input type="radio" value="1" name="GD" id="">Male<br>
            <input type="radio" value="2" name="GD" id="">Female<br>
            <input type="radio" value="3" name="GD" id="">Other<br>
        </p>
        <p><label for="">Nationality</label></p>
        <select name="nat" id="nat">
            <option value="id">Indonesian</option>
            <option value="uk">UK</option>
            <option value="oth">Others</option>
        </select><br>
        <p>Language Spoken:</p>
        <p>
            <input type="checkbox" name="lang-id" id="id">Bahasa Indonesia<br>
            <input type="checkbox" name="lang-en" id="en">English<br>
            <input type="checkbox" name="lang-oth" id="oth">Other<br>
        </p>
        <p><label for="">Bio:</label></p>
        <textarea name="bio" id="bio" cols="40" rows="10"></textarea><br>
        <input type="submit">
    </form>
</body>
</html>
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function form()
    {
        return view('register');
    }

    public function message(Request $request)
    {
        // dd($request->all());
        $nama1 = $request['fname'];
        $nama2 = $request['lname'];
        return view('welcome', ['fname' => $nama1, 'lname' => $nama2]);
    }
}
